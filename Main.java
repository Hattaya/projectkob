
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Main {

   static String Username;
   static String Password;
   static ArrayList<Person> arr = new ArrayList<>();
   static boolean foundID = false, foundPass = false;
   static     Scanner kb = new Scanner(System.in);
   static String a1, a2, a3, a4, e1, e2, e3, e4;
   static double a5, a6, e5, e6;

   
   public static void load(){
       arr.add(new Person("admin","admin","new","new",60.0,162.6));
       arr.add(new Person("bew","1234","bew","gorya",60.0,170));
   }
   

    public static void main (String[] args){
        login();
    }

   
    public static void login() {
        load();
        sayLogin();
        inputLogin();
        checkUser();
    }
    
    public static void sayLogin(){
        System.out.println("Login Panal");
    }
    
    public static void inputLogin(){
        System.out.println("Please input username or password");
        System.out.print("Username : ");    Username = kb.next();
        System.out.print("Password : ");  Password = kb.next();
    }
    
       public static void checkUser(){
            for(Person a : arr){
                if(Username.equals(a.getUser())){
                foundID = true;
                    if(Password.equals(a.getPassword())){
                        foundPass = true;
                        break;
                    }else{
                        foundPass = false;
                        break;
                    }
          }else{
                    foundID =false;
                }   
      }
        checkUserFound();
    }

    public static void checkUserFound() {
        if(foundID == true && foundPass == true){
            showSuccess();
            sayMenu();
        }else if(foundID == true && foundPass == false){
           showErrorLoginPass();
           sayLogin();
            inputLogin();
        }else{
            showErrorLoginUser();
            sayLogin();
            inputLogin();
        }
    }

    private static void showSuccess() {
        System.out.println("Success.");
    }

    private static void sayMenu() {
        System.out.println("Menu Panel");
        System.out.println("1.  Add");
        System.out.println("2.  Show");
        System.out.println("3.  Edit");
        System.out.println("4.  Log out");
        System.out.println("5.  Exit");
        System.out.println("");
        checkMenu();
    }

    private static void checkMenu() {
        showPleaseChoose();
        int input = kb.nextInt();
       switch (input) {
       case 1:
           sayAdd();
           break;
       case 2:
           sayShow();
           break;
       case 3:
           sayEditCheckPass();
           break;
       case 4:
           logOut();
           break;
       case 5:
           System.exit(0);
           break;
       default:
            showErrorInput();
            sayMenu();

           break;
       }
    }

    private static void showErrorLoginPass() {
      System.err.println("Password is incorrect.");
    }

    private static void showErrorLoginUser() {
        System.err.println("User is incorrect.");
    }

    private static void showPleaseChoose() {
        System.out.println("Please choose : ");
        
    }

    private static void sayAdd() {
        System.out.println("Please input information");
        System.out.println("1. Username 2. Password 3. Firstname 4. Lastname 5. Weight 6. Height");
        System.out.println("(0) back");
        add();
    }

    private static void sayShow() {
       System.out.println("Show Panel");
       int i =1 ;
                for (Person a :arr) {
                    System.out.println(i+". "+a.getName()+" "+a.getSurname());
                    i++;
                }
                    System.out.println("(0) back");
                    show();

    }

    private static void sayEditCheckPass() {
               editCheckPass();
                showPleaseChoose();
                String input = kb.next();
                if (input.equals("0")) {
                       sayMenu();
	} else {
                        editPassFound(input);
	}
    }

    private static void logOut() {
                System.out.println("Do you want to logout?(Y/N)");
                showPleaseChoose();
	String input = kb.next();
	if (input.equalsIgnoreCase("Y")) {
                        sayLogin();
	} else if (input.equalsIgnoreCase("N")) {
                        sayMenu();
	} else
                        logOut();
    }

    private static void showErrorInput() {
        System.err.println("Input didn't match.");
    }

    private static void add() {
        chooseAddUser();
        chooseAddPass();
        chooseAddFname();
        chooseAddLname();
        chooseAddWeight();
        chooseAddHeight();
        addSave();

    }

    private static void show() {
        boolean check = false;
        int input;
        while (!check) {
            try {
                showPleaseChoose();
                input = kb.nextInt();
                checkShow(input);
            } catch (InputMismatchException e) {
                showErrorShowInput();
                kb.next();
            }
        }
    }

    private static void editCheckPass() {
        System.out.println("Please input your password");
        System.out.println("(0) back");

    }

    private static void editPassFound(String input) {
        System.out.println("editPassFound");
    }

    private static void chooseAddUser() {
        System.out.print("username : ");
        a1 = kb.next();
        if (a1.equals("0")) {
            sayMenu();
        } else {
            for (Person a : arr) {
                if (a.getUser().equals(a1)) {
                    showErrorAddUser();
                    chooseAddUser();
                }
            }
            System.out.println("Username can use.");
        }

    }

    private static void chooseAddPass() {
        System.out.print("password : ");
        a2 = kb.next();
        if (a2.equals("0"))
            sayMenu();
    }

    private static void chooseAddFname() {
        System.out.print("firstname : ");
        a3 = kb.next();
        if (a3.equals("0"))
            sayMenu();
    }

    private static void chooseAddLname() {
        System.out.print("lastname : ");
        a4 = kb.next();
        if (a4.equals("0"))
            sayMenu();
    }

    private static void chooseAddWeight() {
        boolean check = false;
        while (!check) {
            try {
                System.out.print("weight (cannot be zero) : ");
                a5 = kb.nextDouble();
                if (a5 == 0) {
                    sayMenu();
                    break;
                }
                check = true;
            } catch (InputMismatchException e) {
                showErrorWeight();
                kb.next();
            }
        }
        System.out.println("Weight can use.");
    }

    private static void chooseAddHeight() {
        boolean check = false;
        while (!check) {
            try {
                System.out.print("height (cannot be zero) : ");
                a6 = kb.nextDouble();
                if (a6 == 0) {
                    sayMenu();
                    break;
                }
                check = true;
            } catch (InputMismatchException e) {
                showErrorHeight();
                kb.next();
            }
        }
        System.out.println("Height can use.");
    }

    private static void addSave() {
      System.out.println("(0) back / (Y) save");
	showPleaseChoose();
	String input = kb.next();
	if (input.equalsIgnoreCase("Y")) {
                            arr.add(new Person(a1, a2, a3, a4, a5, a6));
                            showSuccess();
                            sayMenu();
	} else if (input.equals("0")) {
                            sayAdd();
	} else{
                            addSave();
                    }
    }

    private static void showErrorAddUser() {
        System.err.println("If Username has been exist. This message will show.");
    }

    private static void showErrorWeight() {
       System.err.println("Weight must be number.");
    }

    private static void showErrorHeight() {
       System.err.println("Height must be number.");
    }

    private static void checkShow(int input) {
        for (int i = 0; i < arr.size(); i++) {
            if (input == i + 1) {
                System.out.println(arr.get(i));
                sayShow();
                break;
            } else if (input == 0) {
                sayMenu();
                break;
            } else if (input > arr.size()) {
                showErrorInput();
                sayShow();
                break;
            }
        }
        showErrorInput();
        sayShow();
    }

    private static void showErrorShowInput() {
        System.err.println("Weight must be number.");
    }
 
}

class Person{
    private String user;
     private String password;
     private String name;
     private String surname;
     private double weight;
     private double hight;
     
     Person(String user,String password,String name,String surname,double weight,double hight){
         this.user = user;
         this.password = password;
         this.name = name;
         this.surname = surname;
         this.weight = weight;
         this.hight = hight;
         }
     
     public  String getUser(){
         return user;
     }
     
     public  String getPassword(){
         return password;
     }
     
     public  String getName(){
         return name;
     }
     
     public  String getSurname(){
         return surname;
     }
     
     public  double getWeight(){
         return weight;
     }
     
     public  double getHight(){
         return hight;
     }
     
}

